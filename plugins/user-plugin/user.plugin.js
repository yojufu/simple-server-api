const passport = require('passport')
const bcrypt = require('bcrypt-nodejs')
const JwtStrategy = require('passport-jwt').Strategy
const ExtractJwt = require('passport-jwt').ExtractJwt
const jwt = require('jsonwebtoken');

module.exports = class UserPlugin {
    constructor() {
        this.name = 'User Plugin'
        this.author = 'Harry Horton'
        this.description = 'Add user capabilities to server'
        this.dependencies = []
    }

    run(app) {
        this.setupUserModel(app)
        this.setupPassport(app)
        this.setupRoutes(app)
    }

    setupUserModel(app) {
        //user model setup
        app.addModel('User', {
                schema: {
                    local: {
                        email: String,
                        password: String,
                    },
                    emailVerified: {
                        type: Boolean,
                        default: false
                    },
                    signupDate: {
                        type: Date,
                        default: Date.now
                    },
                    role: {
                        type: String,
                        enum: ['user', 'superadmin'],
                        default: 'user'
                    }
                },
                methods: {
                    generateHash(password) {
                        return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null)
                    },
                    validPassword(password) {
                        return bcrypt.compareSync(password, this.local.password);
                    }
                }
            }

        )
    }

    setupRoutes(app) {
        let User = app.model('User')
        app.post('/signup', function (req, res, next) {
            User.findOne({
                'local.email': req.body.email
            }, function (err, user) {
                // if there are any errors, return the error
                if (err) {
                    res.json({
                        error: "There is a problem with the server.  Please try again later."
                    })
                } else if (user) {
                    res.json({
                        error: req.body.email + " is already in use."
                    })
                } else if (!req.body.password) {
                    res.json({
                        error: "Please enter a password"
                    })
                } else {
                    var newUser = new User();

                    // set the user's local credentials
                    newUser.local.email = req.body.email;
                    newUser.local.password = newUser.generateHash(req.body.password);

                    // save the user
                    newUser.save(function (err) {
                        if (err)
                            throw err;

                        res.json({
                            user: newUser,
                            redirect: '/account/login'
                        })
                    });
                }

            });
        });

        app.post('/login', function (req, res, next) {
            User.findOne({
                'local.email': req.body.email
            }, function (err, user) {
                if (err) {
                    res.json({
                        error: "There is a problem with the server.  Please try again later."
                    })
                } else if (!user) {
                    res.json({
                        error: "A user with that name was not found"
                    })
                } else if (!req.body.password) {
                    res.json({
                        error: "Please enter a password"
                    })
                } else if (!user.validPassword(req.body.password)) {

                    res.json({
                        error: "The password you entered was incorrect"
                    })
                } else {
                    //TODO: don't include entire user, just id, email, roles, something like that.
                    let token = jwt.sign(user, 'secret', {
                        expiresIn: 10080 // in seconds
                    });
                    //using cookies instead
                    //todo:  find out if maxAge here matters when compared to jwt token.
                    res.cookie('jwt', token, {
                        maxAge: 900000,
                        httpOnly: true
                    });
                    res.json({
                        token,
                        user
                    });
                }

            });
        });

        app.get('/logout', function (req, res, next) {
            res.clearCookie('jwt');
            res.json({
                message: 'successfully logged out'
            });
        });

        app.get('/profile', app.passport.authenticate('jwt', {
            session: false
        }), function (req, res) {
            //TODO:  Returns unauthorized, find out where this happens and add redirec to login key.

            res.json(req.user);
        });
    }
    setupPassport(app) {
        //setup passport
        app.passport = passport

        app.express.use(passport.initialize())
        app.express.use(passport.session())
        //jwt setup
        var opts = {};
        opts.jwtFromRequest = ExtractJwt.fromExtractors([function (req) {
            var token = null;
            if (req && req.cookies) {
                token = req.cookies['jwt'];
            }
            return token;
        }, function (req) {
            var token = null
            if (req && req.body && req.body.token) {
                token = req.body.token
            }
            return token
        }]);
        opts.secretOrKey = 'secret';
        //strategy
        app.passport.use(new JwtStrategy(opts, function (jwt_payload, done) {
            let User = app.model('User')
            User.findOne({
                id: jwt_payload.id
            }, function (err, user) {
                if (err) {
                    return done(err, false);
                }
                if (user) {
                    done(null, user);
                } else {
                    done(null, false);
                }
            });
        }));
    }
}