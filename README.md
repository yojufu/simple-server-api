# CMS Server API #

This is the core server API intended to be the base for the modern NodeJs CMS project.

Note: The only const === change 

## Project Description

The primary goal of this module is to provide a dead simple interface for quickly spinning up a fully functional web server including APIs for"

- Action hooks and filters
- Mongoose Models
- Express server
- Routing system and controllers
- Passport authentication

`server.js` is the primary export file for the module.  The `Server` class imports it's core functionality from modules in the `/modules` folder.

### Architecture description
Entrypoint: `new Server()` from server.js

The `Server` class on initialization adds it's own core modules in the order of dependencies.

Using the `use` call, each module's methods are merged into the `Server` class. Their methods are
 merged into the `Server` class, and initializing code (including adding instance variables) are called by the module's `install()` method.  This allows modules to have the ability to be self organized for testing, but still enable extension of the master `Server` class.

 The `ActionModule` is the most important.  Modules and plugins need to be extendable, and have code that runs at specific points in the application lifecycle without needing to modify core code.  `Server` contains two core actions: `RUN_PLUGINS` and `START_SERVER` (note that the list may grow).  Modules and Plugins can hook their initialization code into eachother's action's, or into the `Server`'s.

#### Core hooks
 - `ADD_ROUTE` in `exrpress.module` is used by the route module to add routes.
 - `RUN_PLUGINS` in `Server` is intended for plugin setup.
 - `START_SERVER` in `Server` is intended for code that runs when all configurations are extended and complete. 

### Files
- `server.js` -
- `/use-extender/index` - Adds class extension/merging functionality
- `action.module.js` - Action hooks api
- `express.module.js` - Base setup for express server
- `model.module.js` - Mongoose interface
- `route.module.js` - Express Routes interface
- `plugin.module.js` - Adds plugin functionality

### Internal plugins

#### User Plugin
Adds Model and routes for basic user creation, login, logout, and authentication. This
Plugin currently houses a base passport JWT setup.

## TODO
- Complete routing module by integrating express.use functionality
  - Add an `/api/` route
  - Move user plugin to an `/api/auth/` route
  - Make sure that routes can require authentication
  - Add route authorization
- Add user roles and authorization to `user-plugin`
- Add passport extendability and/or replacement to `user-plugin`
- Add templating system
- Accept all relevant setup variables in `new server(options)`
- Set up project to work as NPM package
- Set up baseline for testing and begin testing features when baseline is set.
- Set up Docker container for testing against.
- Add example configuration file and `.gitignore` one that is used.  Pass into server
  from running directory (root).
- Add model normalization functions for model extension additions and removals
- Investigate adding routes dynamically
