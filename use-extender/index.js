module.exports = function use(extension, options) {
    if (typeof extension.prototype !== 'undefined') {
        Object.getOwnPropertyNames(extension.prototype).forEach(proto => {
            if (proto !== 'install' && proto !== 'constructor') this[proto] = extension.prototype[proto]
        })
        if (extension.prototype.install) extension.prototype.install.apply(this, [options])
    } else {
        Object.keys(extension).forEach(key => {
            if (key !== 'install') this[key] = extension[key]
        })
        if (extension.install) extension.install.apply(this, [options])
    }
}