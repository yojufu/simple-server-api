/** 
 * Action represents individual server lifecycle hook
 * @access private
 */
class Action {
    /**
     * @ignore
     */
    constructor() {
        this.functionList = []
    }
    /**
     * Adds passed function to queued list.
     * @param {function} func 
     */
    addFunction(func) {
        this.functionList.push(func)
    }
    /**
     * Runs all functions with this context padding data through each.
     * @param {Object} context - this context for runnable function 
     * @param {*} data - data passed to the queued functions
     * @returns {*} returns potentially mutated data after queue
     */
    run(context, data) {
        this.functionList.forEach(func => {
            let result = func.apply(context, [data])
            if (typeof result !== 'undefined') {
                data = result
            }
        })
        return data
    }
}

/**
 * Action Module - adds action hooks
 */
module.exports = class ActionModule {
    /**
     * @ignore
     */
    install() {
        this.actions = new Map()
    }
    /**
     * Runs all queued functions for existing action name.
     * @param {string} actionName - name of action to run.
     * @param {*} data - date to pass into action run.
     * @return {*} - result of action function queue.
     */
    doAction(actionName, data) {
        if (this.actions.has(actionName)) {
            return this.actions.get(actionName).run(this, data)
        }
    }
    /**
     * Adds function to action name and creates action if nonexistent.
     * @param {string} actionName - action name to add to.
     * @param {*} func - function to be queued.
     */
    addAction(actionName, func) {
        if (!this.actions.has(actionName)) {
            this.actions.set(actionName, new Action())
            this.actions.get(actionName).addFunction(func)
        } else {
            this.actions.get(actionName).addFunction(func)
        }
    }
}