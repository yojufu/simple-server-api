const mongoose = require('mongoose')
const Schema = mongoose.Schema

/**
 * Interface for extendable Mongoose models
 */
module.exports = class ModelModule {
    /**
     * @ignore
     */
    install() {
        this.models = new Map()
        this.mongoose = mongoose
        this.mongoose.Promise = global.Promise
        this.addAction('START_SERVER', this.startMongo)
    }
    /**
     * 
     * @param {string} modelName - name of model
     * @param {Object} config - model configuration
     * @example
     * ```
     * this.addModel('User', {
     *   schema:{
     *     username: string
     *   }
     * })
     * ```
     */
    addModel(modelName, config) {
        if (!this.models.has(modelName)) {
            this.models.set(modelName, config)
        } else {
            throw new Error(`Model ${modelName} already exists.`)
        }
    }
    /**
     * Merges new model definition with existing overriding old values.
     * @param {string} modelName - name of model
     * @param {Object} config - model configuration
     */
    extendModel(modelName, config) {
        if (this.models.has(modelName)) {
            let model = this.models.get(modelName)
            Object.assign(model, config)
        } else {
            throw new Error(`Model ${modelName} cannot be extended as it does not exist.`)
        }
    }
    /**
     * Creates and returns a mongoose model object based on added and extended models.
     * Splits the module configuration and appropriately applies schema, methods, statics,
     * query, and virtuals.
     * @param {string} modelName
     * @todo Figure out schema.index and apply appropriately
     * @returns {Object} Mongoose model
     */
    model(modelName) {
        if (this.models.has(modelName)) {
            let model = this.models.get(modelName)
            let options = model.options || {}
            let schema = new Schema(model.schema, options)
            if (model.methods) schema.methods = model.methods
            if (model.statics) schema.statics = model.statics
            if (model.query) schema.query = model.query
            if (model.virtuals) {
                for (let virtual in model.virtuals) {
                    schema.virtual(virtual).get(model.virtuals[virtual].get).set(model.virtuals[virtual].set)
                }
            }
            return this.mongoose.model(modelName, schema)
        } else {
            throw new Error(`Model ${modelName} does not exist.`)
        }

    }
    /**
     * Starts mongo database connection
     */
    startMongo() {
        let serverOptions = this.options
        this.mongoose.connect('mongodb://' + serverOptions.dbLocation + "/" + serverOptions.dbName)
            .then(function () {
                console.log(`MongoDb: ${serverOptions.dbLocation}/${serverOptions.dbName} Connected`)
            }).catch(function (err) {
                console.error(`MongoDb: not connected: ${err.message}`)
            })
    }

}