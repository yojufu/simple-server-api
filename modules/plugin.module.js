/**
 * Reorders plugins based on their dependencies
 * @param {Array} pluginList - list of plugins to reorder
 * @return {Array} sorted plugin list
 */
function reorderDependencies(pluginList) {
    console.log(pluginList)
    let runList = pluginList.sort(function (plug1, plug2) {
        if (!plug1.dependendices || plug1.dependendices.length === 0) {
            return 1
        } else {
            if (plug1.dependencies.indexOf(plug2.name) > -1) {
                return -1
            } else if (plug2.dependencies.indexOf(plug1.name) > 1) {
                return 1
            }
        }
    })
    return runList
}

/**
 * Plugin module contains plugin extention capabilities.
 */
module.exports = class PluginModule {
    install() {
        this.plugins = new Map()
        this.addAction('RUN_PLUGINS', this.runPlugins)
    }
    /**
     * Adds a plugin to the application
     * @param {Object} newPlugin 
     */
    usePlugin(newPlugin) {
        let plugin = new newPlugin()
        this.plugins.set(plugin.name, plugin)
    }
    /**
     * Removes a plugin from the application run list
     * @param {string} pluginName 
     */
    disablePlugin(pluginName) {
        if (this.plugins.has(pluginName)) this.plugins.delete(pluginName)
    }
    /**
     * Removes a plugin from the application
     * @param {string} pluginName 
     */
    uninstallPlugin(pluginName) {
        if (this.plugins.has(pluginName)) {
            let plugin = this.plugins.get(pluginName)
            plugin.uninstall(app)
            this.plugins.delete(pluginName)
        }
    }
    /**
     * Runs all plugins
     */
    runPlugins() {
        let pluginList = Array.from(this.plugins.values())
        pluginList = reorderDependencies(pluginList)
        pluginList.forEach(plugin => {
            plugin.run(this)
        })
    }
    getPlugin(pluginName) {
        return this.plugins.get(pluginName)
    }
    getPlugins() {
        return this.plugins.keys()
    }

}