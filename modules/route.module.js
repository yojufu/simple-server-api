const express = require('express')
const router = express.Router()

/**
 * Route module adds interface to express routing system.
 */
module.exports = class RouteModule {
    /**
     * @ignore
     */
    install() {

    }
    /**
     * Access to express get(). Adds to ADD_ROUTE action.
     */
    get() {
        let args = arguments
        this.addAction('ADD_ROUTE', () => {
            this.express.get(...args)
        })
    }
    /**
     * Access to express post(). Adds to ADD_ROUTE action.
     */
    post() {
        let args = arguments
        this.addAction('ADD_ROUTE', () => {
            this.express.post(...args)
        })
    }
    /**
     * Access to express delete(). Adds to ADD_ROUTE action.
     */
    delete() {
        let args = arguments
        this.addAction('ADD_ROUTE', () => {
            this.express.delete(...args)
        })
    }
    /**
     * Access to express put(). Adds to ADD_ROUTE action.
     */
    put() {
        let args = arguments
        this.addAction('ADD_ROUTE', () => {
            this.express.put(...args)
        })

    }
    /**
     * Access to express router's use module.
     * @todo 
     * Determine how to allow express.use() statements
     * as well as string ...function combo that passes route to the function.
     */
    route() {

    }

}