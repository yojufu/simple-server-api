const config = require('../config/config.json')
const express = require('express');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const session = require('express-session')
const flash = require('connect-flash')
const Use = require('../use-extender/index')
const http = require('http')
const debug = require('debug')('new-folder-(2):server');

//Normalize a port into a number, string, or false.
        function normalizePort(val) {
            let port = parseInt(val, 10);
            if (isNaN(port)) return val;
            if (port >= 0) return port;
            return false;
        }

module.exports = class ExpressModule {
    install() {
        this.use = Use
        this.setupExpress()
        this.expressStarted = false
        this.addAction('START_SERVER', this.startExpress)
    }

    setupExpress() {
        this.express = express()

        // view engine setup
        this.express.set('views', path.join(__dirname, 'views'));
        this.express.set('view engine', 'hbs');
        this.express.use(logger('dev'));
        this.express.use(bodyParser.json());
        this.express.use(bodyParser.urlencoded({
            extended: true
        }));
        this.express.use(cookieParser(config.secret));
        this.express.use(session({
            secret: config.secret,
            resave: false,
            saveUninitialized: true
        }))
        this.express.use(flash())
        this.express.use(express.static(path.join(__dirname, 'public')));

        
    }
    setupExpressRoutes(){
        this.doAction('ADD_ROUTE')

        //routes registry
        //require('./routes/routes.registry')(this.express, passport)

        // catch 404 and forward to error handler
        this.express.use(function (req, res, next) {
            let err = new Error('Not Found');
            err.status = 404;
            next(err);
        });

        // error handler
        this.express.use(function (err, req, res, next) {
            // set locals, only providing error in development
            res.locals.message = err.message;
            res.locals.error = req.app.get('env') === 'development' ? err : {};

            // render the error page
            res.status(err.status || 500);
            res.send(err.message);
        });
    }
    startExpress() {
        this.setupExpressRoutes()


        //Get port from environment and store in Express.
        let port = normalizePort(process.env.PORT || '3000');
        this.express.set('port', port);
        //set up http service
        this.server = http.createServer(this.express);
        //Listen on provided port, on all network interfaces.
        this.server.listen(port);
   
        this.server.on('error', (error) => {
            if (error.syscall !== 'listen') throw error;

            let bind = typeof port === 'string' ?
                'Pipe ' + port :
                'Port ' + port;

            // handle specific listen errors with friendly messages
            switch (error.code) {
                case 'EACCES':
                    console.error(bind + ' requires elevated privileges');
                    process.exit(1);
                    break;
                case 'EADDRINUSE':
                    console.error(bind + ' is already in use');
                    process.exit(1);
                    break;
                default:
                    throw error;
            }
        });

        this.server.on('listening', () => {
            var addr = this.server.address();
            var bind = typeof addr === 'string' ?
                'pipe ' + addr :
                'port ' + addr.port;
            debug('Listening on ' + bind);
        });
        
    }
}