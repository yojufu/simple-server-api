const Server = require('./server')
const UserPlugin = require('./plugins/user-plugin/user.plugin')
const app = new Server({
            dbLocation: 'localhost:32768',
            dbName: 'cms'
        })

app.usePlugin(UserPlugin)

app.get('/', function (req, res) {
    res.send('ok')
})

app.start()