const Use = require('./use-extender/index')
const ActionModule = require('./modules/action.module')
const ExpressModule = require('./modules/express.module')
const RouteModule = require('./modules/route.module')
const ModelModule = require('./modules/model.module.js')
const PluginModule = require('./modules/plugin.module')

/**
 * @description
 * Server is the main object of the application. It loads in functionality
 * from internal core modules and extends itself with their methods and
 * instance variables.  The server object is the primary entry point for 
 * the entire API.
 */
module.exports = class Server {
    constructor(options) {
        this.use = Use
        this.options = options
        this.use(ActionModule)
        this.use(ModelModule)
        this.use(ExpressModule)
        this.use(RouteModule)
        this.use(PluginModule)
    }
    /**
     * Starts the server.
     */
    start() {
        this.doAction('RUN_PLUGINS')
        this.doAction('START_SERVER')
    }
}